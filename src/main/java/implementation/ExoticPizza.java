package implementation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
/**
 * Created by KW on 10/11/2017.
 */
@Primary
@Component
public class ExoticPizza implements IPizza {
    private String name;
    private int price;

    public ExoticPizza(@Value("Egzotyczna") String name,@Value("100") int price) {
        this.name = name;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

}
