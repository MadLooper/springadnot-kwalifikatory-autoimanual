package implementation;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by KW on 10/11/2017.
 */
@Component
@Qualifier("Dobra")
public class GoodPizza implements IPizza {
    private String name;
    private int price;

    public GoodPizza(@Value("dobra") String name,@Value("15") int price) {
        this.name = name;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

}
