package implementation;

import implementation.IOrder;
import implementation.IPizza;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
/**
 * Created by KW on 10/11/2017.
 */
public class Order implements IOrder {

    private IPizza pizza;
//spring sam znajdzie zadeklarowana pizze jako komponent i ja wstrzyknie
    public Order(@Qualifier("Dobra") IPizza pizza) {
        super();
        this.pizza = pizza;
    }

    public void printOrder() {
        System.out.println("Zamowienie:  "+ pizza.getName() + "  w cenie " + pizza.getPrice());
    }
}
