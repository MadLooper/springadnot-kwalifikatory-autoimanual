package pizzeria;

import implementation.IOrder;
import implementation.Order;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import config.Autoconfig;

/**
 * Created by KW on 10/11/2017.
 */
public class Main {
    public static void main(String[] args) {
       // AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(config.class);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Autoconfig.class);
    IOrder order = context.getBean(Order.class);
    order.printOrder();
    }
}
//oznaczenie, ktora pizza ma byc wstrzyknieta, gdy sa wstawione obie:
//na poczatku dajesz @Primary, tam gdzie ma byc wstrzyknieta i w konstrukrorze definiujesz value, lub
//@Qualifier - definiujesz w klasie,ktora ma byc wstrzyknieta(np.dobra pizza) i w Order w konstruktorze